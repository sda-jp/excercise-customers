package pl.sda.jp.excercise.customers;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CustomerService {

    public List<Customer> convertToList(Customer[] customers) {
        if (customers == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(customers);
    }

    public List<String> convertToListOfNames(Customer[] customers) {
        return Optional.ofNullable(customers)
                .map(ca -> Arrays.stream(ca)
                        .map(c -> c.getFirstName() + " " + c.getLastName())
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    public Map<Integer, Customer> convertToMap(Customer[] customers) {
        return Optional.ofNullable(customers)
                .map(ca -> Arrays.stream(ca)
                        .collect(Collectors.toMap(Customer::getId, Function.identity())))
                .orElse(Collections.emptyMap());
    }

    public Map<BigDecimal, List<Customer>> convertToSalaryMap(Customer[] customers) {
        return Optional.ofNullable(customers)
                .map(ca -> Arrays.stream(ca)
                        .collect(Collectors.groupingBy(Customer::getSalary)))
                .orElse(Collections.emptyMap());
    }

    public BigDecimal computeAllWishItemsCost(Customer customer, Map<String, BigDecimal> carOptionNamePriceMap) {
        return customer.getWishList().stream()
                .map(wishItem -> carOptionNamePriceMap.get(wishItem.getName()).multiply(new BigDecimal(wishItem.getQuantity())))
                .reduce(BigDecimal.ZERO, (p1, p2) -> p1.add(p2));
    }

    public List<WishItem> getAffordableItemsIncludingOrder(Customer customer, Map<String, BigDecimal> carOptionNamePriceMap) {
        BigDecimal itemsCost = BigDecimal.ZERO;
        List<WishItem> affordableWhishItems = new ArrayList<>();

        for (WishItem wishItem : customer.getWishList()) {
            itemsCost = itemsCost.add(getWishItemFinalPrice(carOptionNamePriceMap, wishItem));
            if (customer.getSalary().compareTo(itemsCost) >= 0) {
                affordableWhishItems.add(wishItem);
            } else {
                break;
            }
        }

        return affordableWhishItems;

    }

    public List<WishItem> getAffordableItemsMaxQuantity(Customer customer, Map<String, BigDecimal> carOptionNamePriceMap) {
        BigDecimal itemsCost = BigDecimal.ZERO;
        List<WishItem> affordableWhishItems = new ArrayList<>();
        List<WishItem> wishItemsSorted = customer.getWishList().stream().sorted(Comparator.comparing(i -> getWishItemFinalPrice(carOptionNamePriceMap, i))).collect(Collectors.toList());

        for (WishItem wishItem : wishItemsSorted) {
            itemsCost = itemsCost.add(getWishItemFinalPrice(carOptionNamePriceMap, wishItem));
            if (customer.getSalary().compareTo(itemsCost) >= 0) {
                affordableWhishItems.add(wishItem);
            } else {
                break;
            }
        }

        return affordableWhishItems;

    }

    private BigDecimal getWishItemFinalPrice(Map<String, BigDecimal> carOptionNamePriceMap, WishItem wishItem) {
        return new BigDecimal(wishItem.getQuantity()).multiply(carOptionNamePriceMap.get(wishItem.getName()));
    }
}
