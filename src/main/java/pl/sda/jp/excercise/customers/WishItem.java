package pl.sda.jp.excercise.customers;

import lombok.Getter;

@Getter
public class WishItem {
    private final String name;
    private final Integer quantity;

    public WishItem(String name, Integer quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "{" +
                name + ": " +
                quantity +
                '}';
    }
}
