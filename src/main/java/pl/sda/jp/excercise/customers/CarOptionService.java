package pl.sda.jp.excercise.customers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class CarOptionService {

    public Map<String, BigDecimal> convertToNamePriceMap(CarOption[] carOptions) {
        return Optional.ofNullable(carOptions)
                .map(c -> Arrays.stream(c).collect(Collectors.toMap(CarOption::getName, CarOption::getPrice)))
                .orElse(Collections.emptyMap());
    }
}
