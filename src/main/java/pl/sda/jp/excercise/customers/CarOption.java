package pl.sda.jp.excercise.customers;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class CarOption {
    private Integer id;
    private String name;
    private BigDecimal price;

    private static int sequenceGenerator = 1;

    public CarOption(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
        this.id = ++sequenceGenerator;
    }

    public CarOption(String name, int price) {
        this(name, new BigDecimal(price));
    }
}
